# {{cookiecutter.Title}}

{{cookiecutter.Description}}{% if cookiecutter.Template in ('Godot', 'Godot (Precompiled)') %}

## Godot Instructions

Following the instructions on the
[Godot Documentation](https://docs.godotengine.org/en/stable/getting_started/workflow/export/exporting_pcks.html#generating-pck-files),
export your project as a PCK called "{{cookiecutter['App Name']}}.pck" and
adapt the clickable.yaml to install that one instead of the demo package.{% endif %}{% if cookiecutter.License != 'Not open source' %}

## License

Copyright (C) {{cookiecutter['Copyright Year']}}  {{cookiecutter['Maintainer Name']}}{% if cookiecutter.License == 'GNU General Public License v3' %}

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License version 3, as published by the
Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY
QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.{% else %}

Licensed under the {{cookiecutter.License}}.{% endif %}{% endif %}
